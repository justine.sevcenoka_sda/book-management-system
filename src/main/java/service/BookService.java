package service;

import domain.Author;
import domain.Book;
import repository.AuthorHiberRepository;
import repository.BookHiberRepository;

public class BookService {

    private BookHiberRepository bookHiberRepository;
    private AuthorHiberRepository authorHiberRepository;

    public BookService(BookHiberRepository bookHiberRepository, AuthorHiberRepository authorHiberRepository) {
        this.bookHiberRepository = bookHiberRepository;
        this.authorHiberRepository = authorHiberRepository;
    }


    public void save(String title, String description, Integer authorId) {
        Author author = authorHiberRepository.findById(authorId);
        Book book = new Book();
        book.setTitle(title);
        book.setDescription(description);
        book.setAuthor(author);
        bookHiberRepository.createBook(book);
    }


}
