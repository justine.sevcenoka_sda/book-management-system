package service;

import domain.Author;
import repository.AuthorHiberRepository;

import java.util.List;

public class AuthorService {

    public static final String VALIDATION_FAILED_MSG = "";

    private AuthorHiberRepository authorHiberRepository;

    public AuthorService(AuthorHiberRepository authorHiberRepository) {
        this.authorHiberRepository = authorHiberRepository;
    }

    public String save(String firstName, String lastName) {
        if ("".equals(firstName) || "".equals(lastName)) {
            return "Empty names";
        }

        Author author = new Author();
        author.setFirstName(firstName);
        author.setLastName(lastName);
        return authorHiberRepository.save(author) ? "Saved" : "Not Saved";
    }

    public List<Author> findAll() {
        return authorHiberRepository.findAll();
    }
}
