package ui;

import javax.swing.*;
import java.awt.*;

public class MainView {
    AuthorView authorView;
    BookView bookView;
    ReviewView reviewView;
    JPanel authorPanel;
    JPanel bookPanel;

    public MainView(AuthorView authorView, BookView bookView, ReviewView reviewView) {
        this.authorView = authorView;
        this.bookView = bookView;
        this.reviewView = reviewView;
    }

    public void show() {
        JFrame frame = new JFrame();
        JPanel mainPanel = new JPanel();

        addProgrammTitle();

        JButton authorButton = new JButton("Author");
        authorButton.addActionListener(actionEvent -> {
            if (authorView.authorPanelIsVisible) {
                setAuthorPanelInvisible();
            } else {
                if(bookView.bookPanelIsVisible) {
                    setBookPanelInvisible();
                }
                authorPanel = authorView.getMainPanel();
                mainPanel.add(authorPanel);
                authorView.authorPanelIsVisible = true;
                frame.show();
            }
        });

        JButton bookButton = new JButton("Book");
        bookButton.addActionListener(actionEvent -> {
            if (bookView.bookPanelIsVisible) {
                setBookPanelInvisible();
            } else {
                if(authorView.authorPanelIsVisible){
                    setAuthorPanelInvisible();
                }

                bookPanel = bookView.getPanel();
                mainPanel.add(bookPanel);
                bookView.bookPanelIsVisible = true;
                frame.show();
            }
        });


        JButton reviewButton = new JButton("Review");

        mainPanel.add(authorButton);
        mainPanel.add(bookButton);
        mainPanel.add(reviewButton);

        frame.add(mainPanel);
        frame.setSize(700, 200);
        frame.show();
    }

    private void addProgrammTitle() {
        JLabel titleOfTheProgram = new JLabel("Book Management System");
        Font font = new Font("Courier", Font.BOLD,12);
        titleOfTheProgram.setFont(font);
    }

    private void setBookPanelInvisible() {
        bookPanel.setVisible(false);
        bookView.bookPanelIsVisible = false;
    }

    private void setAuthorPanelInvisible() {
        authorPanel.setVisible(false);
        authorView.authorPanelIsVisible = false;
    }
}
