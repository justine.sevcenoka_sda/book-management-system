package ui;

import service.AuthorService;

import javax.swing.*;
import java.awt.*;

public class AuthorView {

    private final int PANEL_WIDTH = 1300;
    private final int PANEL_HEIGHT = 300;
    boolean authorPanelIsVisible;
    private JPanel mainPanel;
    private AuthorService authorService;

    private JLabel firstNameLabel;
    private JLabel lastNameLabel;
    private JLabel saveResultLabel;
    private JTextField firstNameTextField;
    private JTextField lastNameTextField;
    private JButton saveButton;
    private JList authorList;

    public AuthorView(AuthorService authorService) {
        this.authorService = authorService;
        authorPanelIsVisible = false;
        generatePanel();
    }

    private void generatePanel() {
        firstNameLabel = new JLabel("First Name");
        lastNameLabel = new JLabel("Last Name");
        saveResultLabel = new JLabel("");

        firstNameTextField = new JTextField("", 8);
        lastNameTextField = new JTextField("", 8);

        DefaultListModel lm = new DefaultListModel();
        lm.addAll(authorService.findAll());
        authorList = new JList(lm);


        saveButton = new JButton("Save");
        saveButton.addActionListener(actionEvent -> {
            if (firstNameTextField.getText().length() > 0 && lastNameTextField.getText().length() > 0) {

                String firstName = firstNameTextField.getText();
                String lastName = lastNameTextField.getText();
                String result = authorService.save(firstName, lastName);
                saveResultLabel.setText(result);

//                Timer timer = new Timer(100 ,taskPerformer);
//                timer.setRepeats(false);
//                timer.start();
//
//                Thread.sleep(5000);

                lastNameTextField.setText("");
                firstNameTextField.setText("");
            }
            mainPanel.setVisible(false);
            authorPanelIsVisible = false;
        });

        mainPanel = generatePanelWithGridLayout();

    }

    private JPanel generatePanelWithGridLayout() {
        JPanel p = new JPanel();

        p.setSize(PANEL_WIDTH, PANEL_HEIGHT);
        GridBagLayout layout = new GridBagLayout();
        p.setLayout(layout);
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.HORIZONTAL;

        addComponentToPanel(firstNameLabel, p, gbc, 0, 0);
        addComponentToPanel(firstNameTextField, p, gbc, 1, 0);
        addComponentToPanel(lastNameLabel, p, gbc, 0, 1);
        addComponentToPanel(lastNameTextField, p, gbc, 1, 1);
        addComponentToPanel(saveButton, p, gbc, 0, 2);
        addComponentToPanel(saveResultLabel, p, gbc, 1, 2);
        gbc.gridwidth = 2;

        addComponentToPanel(authorList, p, gbc, 0, 3);
        return p;

    }

    public JPanel getMainPanel() {
        mainPanel.setVisible(true);
        return mainPanel;
    }

    private void addComponentToPanel(JComponent component, JPanel p, GridBagConstraints gbc, int x, int y) {
        gbc.gridx = x;
        gbc.gridy = y;
        p.add(component, gbc);
    }

}
