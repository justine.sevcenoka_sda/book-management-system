package ui;

import service.BookService;

import javax.swing.*;
import java.awt.*;

public class BookView {

    private BookService bookService;
    private JPanel panel;
    boolean bookPanelIsVisible;

    public BookView(BookService bookService) {
        this.bookService = bookService;
        bookPanelIsVisible = false;
        generatePanel();
    }

    private void generatePanel() {

        JLabel titleLabel = new JLabel("Title");
        JLabel descriptionLabel = new JLabel("Description");
        JLabel authorLabel = new JLabel("Author ID");
        JLabel saveResultLabel = new JLabel("");

        JTextField titleTextField = new JTextField("", 10);

        JTextField descriptionTextField = new JTextField("", 20);

        JTextField authorIDTextField = new JTextField("", 3);

        JButton saveButton = new JButton("Save");

        saveButton.addActionListener(actionEvent -> {
            if (titleTextField.getText().length() > 0 && descriptionTextField.getText().length() > 0) {
                String title = titleTextField.getText();
                String description = descriptionTextField.getText();
                Integer authorId = Integer.valueOf(String.valueOf(authorIDTextField.getText()));
                bookService.save(title, description, authorId);

                titleTextField.setText("");
                descriptionTextField.setText("");
            }
            panel.setVisible(false);
            bookPanelIsVisible = false;
        });

        FlowLayout experimentLayout = new FlowLayout();
        panel = new JPanel();
        panel.setLayout(experimentLayout);
        panel.setSize(1300, 300);

        panel.add(titleLabel);
        panel.add(titleTextField);

        panel.add(descriptionLabel);
        panel.add(descriptionTextField);

        panel.add(authorLabel);
        panel.add(authorIDTextField);

        panel.add(saveButton);
        panel.add(saveResultLabel);
    }

    public JPanel getPanel() {
        panel.setVisible(true);
        return panel;
    }
}