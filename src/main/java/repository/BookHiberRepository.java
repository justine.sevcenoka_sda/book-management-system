package repository;

import domain.Book;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import utils.HibernateUtils;

import java.util.List;

public class BookHiberRepository {

    public void createBook(Book book) {
        Transaction transaction = null;
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.save(book);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    public List<Book> findAll() {
       try {
           SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
           Session session = sessionFactory.openSession();
           String selectAllBooks = "from Book";
           Query selectQuery = session.createQuery(selectAllBooks);
           List<Book> books = selectQuery.list();
           session.close();
           return books;
       } catch (Exception e) {
           e.printStackTrace();
           return null;
       }
    }

    public Book findById(Integer id) {
        try {
            SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
            Session session = sessionFactory.openSession();
            Book book = session.find(Book.class, id);
            return book;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public void updateBook(Book book) {
        Transaction transaction = null;
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.update(book);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    public void deleteBook (Book book){
        Transaction transaction = null;
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.delete(book);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

}
