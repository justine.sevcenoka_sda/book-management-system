package repository;

import domain.Author;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import utils.HibernateUtils;

import java.util.List;

public class AuthorHiberRepository {

    public void createAuthor(Author author) {
        Transaction transaction = null;
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.save(author);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    public boolean save(Author author) {
        Integer result = null;
        Session session = HibernateUtils.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        result = (Integer) session.save(author);
        transaction.commit();
        return result != null ? true : false;

    }

    public List<Author> findAll() {

        try {
            SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
            Session session = sessionFactory.openSession();
            String selectAllAuthors = "from Author";
            Query selectQuery = session.createQuery(selectAllAuthors);
            List<Author> authors = selectQuery.list();
            session.close();
            return authors;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Author findById(Integer id) {
        try {
            SessionFactory sessionFactory = HibernateUtils.getSessionFactory();
            Session session = sessionFactory.openSession();
            Author author = session.find(Author.class, id);
            return author;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

    public void updateAuthor(Author author) {
        Transaction transaction = null;
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.update(author);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    public void deleteAuthor (Author author){
        Transaction transaction = null;
        try {
            Session session = HibernateUtils.getSessionFactory().openSession();
            transaction = session.beginTransaction();
            session.delete(author);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }


}
