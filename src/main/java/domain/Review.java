package domain;

import lombok.*;

import javax.persistence.*;
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "reviews")
public class Review {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "reviewId")
    private Integer reviewId;

    @Column (name = "score")
    private Integer score;

    @Column(name = "comment")
    private String comment;

    public Integer getReviewId() {
        return reviewId;
    }

    public Integer getScore() {
        return score;
    }

    public String getComment() {
        return comment;
    }

    @ManyToOne
    @JoinColumn (name = "bookId")
    private Book book;

    @Override
    public String toString() {
        return "Review{" +
                "reviewId=" + reviewId +
                ", score=" + score +
                ", comment='" + comment + '\'' +
                '}';
    }
}

