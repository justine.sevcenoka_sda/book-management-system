import repository.AuthorHiberRepository;
import repository.BookHiberRepository;
import repository.ReviewHiberRepository;
import service.AuthorService;
import service.BookService;
import service.ReviewService;
import ui.AuthorView;
import ui.BookView;
import ui.MainView;
import ui.ReviewView;

public class BookManagementSystem {
    private MainView mainView;
    private AuthorService authorService;
    private BookService bookService;
    private AuthorView authorView;
    private BookView bookView;
    private ReviewService reviewService;
    private ReviewView reviewView;

    public BookManagementSystem() {
        authorService = new AuthorService(new AuthorHiberRepository());
        authorView = new AuthorView(authorService);

        bookService = new BookService(new BookHiberRepository(), new AuthorHiberRepository());
        bookView = new BookView(bookService);

        reviewService = new ReviewService(new ReviewHiberRepository());
        reviewView = new ReviewView();

        mainView = new MainView(authorView, bookView, reviewView);
        show();
    }

    private void show(){
        mainView.show();
    }

}
