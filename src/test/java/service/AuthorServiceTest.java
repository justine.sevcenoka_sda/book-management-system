package service;
import org.junit.After;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import repository.AuthorHiberRepository;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AuthorServiceTest {

    private AuthorService authorService;


    @BeforeEach
    public void setUp()  throws Exception {
        AuthorHiberRepository mockRepo = mock(AuthorHiberRepository.class);
        when(mockRepo.save(any())).thenReturn(true);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    void save() {
        assertEquals(AuthorService.VALIDATION_FAILED_MSG, authorService.save("", ""));
        assertEquals("Saved", authorService.save("s", "3"));
    }
}
