package ui;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import service.AuthorService;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

public class AuthorViewTest {
    private AuthorView authorView;

    @Before
    public void setUp() throws Exception {
        AuthorService mockService = mock(AuthorService.class);
        authorView = new AuthorView(mockService);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void getPanel() {
        assertEquals(true, authorView.getMainPanel() != null);
        assertEquals(1300, authorView.getMainPanel().getSize().width);
        assertEquals(300, authorView.getMainPanel().getSize().height);

    }
}